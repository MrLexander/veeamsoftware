﻿using System.IO;
using NUnit.Framework;
using VeeamSoftware.Exceptions;
using VeeamSoftware.Threading;

namespace VeeamSoftware.Zip.Tests
{
    [TestFixture]
    public sealed class ArchiverTests
    {
        [Test]
        public void CompressAndDecompress_Test()
        {
            const string ftString = "ft string";
            const string secString = "sec string";
            var contents = new[] {ftString, secString};
            var tempPath = Path.GetTempFileName();
            File.WriteAllLines(tempPath, contents);

            var compressedFile = Path.GetTempFileName();
            var decompressedFile = Path.GetTempFileName();
            var exceptionHandler = new ExceptionHandler();
            var safeThreadPool = new SafeThreadPool(exceptionHandler);

            var compresser = new Archiver(tempPath, compressedFile, safeThreadPool);
            compresser.Compress();

            var decompresser = new Archiver(compressedFile, decompressedFile, safeThreadPool);
            decompresser.Decompress();

            var decompressedStrings = File.ReadAllLines(decompressedFile);

            File.Delete(tempPath);
            File.Delete(compressedFile);
            File.Delete(decompressedFile);

            CollectionAssert.AreEqual(contents, decompressedStrings);
        }
    }
}