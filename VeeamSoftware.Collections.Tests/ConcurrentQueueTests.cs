﻿using NUnit.Framework;
using VeeamSoftware.Infrastructure.Model;

namespace VeeamSoftware.Collections.Tests
{
    [TestFixture]
    public sealed class ConcurrentQueueTests
    {
        [Test]
        public void DequeueFromEmpty_False()
        {
            var concurrentQueue = new ConcurrentStreamBlockQueue();
            var dequeued = concurrentQueue.TryDequeue(out var _);

            Assert.IsFalse(dequeued);
        }

        [Test]
        public void Empty_IsEmpty_True()
        {
            var concurrentQueue = new ConcurrentStreamBlockQueue();

            Assert.IsTrue(concurrentQueue.IsEmpty());
        }

        [Test]
        public void EnqueueAndDequeue_Test()
        {
            var block = new StreamBlock();

            var concurrentQueue = new ConcurrentStreamBlockQueue();
            concurrentQueue.Eqnueue(block);
            var dequeued = concurrentQueue.TryDequeue(out var dequeuedString);

            Assert.IsTrue(dequeued);
            Assert.AreEqual(block, dequeuedString);
        }

        [Test]
        public void NotEmpty_IsEmpty_False()
        {
            var block = new StreamBlock();

            var concurrentQueue = new ConcurrentStreamBlockQueue();
            concurrentQueue.Eqnueue(block);

            Assert.IsFalse(concurrentQueue.IsEmpty());
        }
    }
}