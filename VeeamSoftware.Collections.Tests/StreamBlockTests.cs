﻿using NUnit.Framework;
using VeeamSoftware.Infrastructure.Model;

namespace VeeamSoftware.Collections.Tests
{
    [TestFixture]
    public sealed class StreamBlockTests
    {
        [Test]
        public void Ctor_Test()
        {
            var bytes = new[] {new byte(), new byte()};

            var streamBlock = new StreamBlock(bytes, 1);

            Assert.AreEqual(bytes, streamBlock.Block);
        }
    }
}