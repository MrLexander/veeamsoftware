﻿namespace VeeamSoftware.Zip
{
    public interface IProcessedInfoHandler
    {
        long OperatedBytesCount { get; }
        long TotalBytes { get; }
    }
}