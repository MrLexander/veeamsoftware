﻿using System;
using System.IO;
using System.Threading;
using Guard;
using JetBrains.Annotations;
using VeeamSoftware.Collections;
using VeeamSoftware.Infrastructure.Model;
using VeeamSoftware.Scaffolding;
using VeeamSoftware.Threading;

namespace VeeamSoftware.Zip
{
    internal class FileReader : Disposable
    {
        [NotNull]
        private readonly ConcurrentStreamBlockQueue _blocks;

        [NotNull]
        private readonly Thread _readerThread;

        [NotNull]
        private readonly object _startSyncRoot = new object();

        private bool _cancellationRequired;
        private bool _readCompleted;
        private int _currentIndex;

        internal FileReader([NotNull] Stream stream, [NotNull] SafeThreadPool safeThreadPool)
        {
            ThrowIf.Argument.IsNull(safeThreadPool, nameof(safeThreadPool));
            ThrowIf.Argument.IsNull(stream, nameof(stream));

            _blocks = new ConcurrentStreamBlockQueue();
            FileStream = stream;
            _readerThread = safeThreadPool.StartOperation(RunRead);
        }

        protected bool IsEofReached => FileStream.Position == FileStream.Length;

        [NotNull]
        protected Stream FileStream { get; }

        internal bool TryReadNext(out StreamBlock block)
        {
            if (_blocks.TryDequeue(out block))
            {
                return true;
            }

            lock (_startSyncRoot)
            {
                while (_blocks.IsEmpty() && !_cancellationRequired && !_readCompleted)
                {
                    Monitor.Wait(_startSyncRoot);
                }
            }

            return _blocks.TryDequeue(out block);
        }

        protected override void FreeManagedResources()
        {
            _cancellationRequired = true;
            const int waitForCancellationTimeout = 1000;
            _readerThread.Join(waitForCancellationTimeout);
            if (_readerThread.IsAlive)
            {
                _readerThread.Abort();
            }
        }
        
        protected virtual bool TryGetNextBlock(out byte[] block)
        {
            const int blockSize = CompressionConstants.DefaultSizeInBytes;
            var buffer = new byte[blockSize];
            var bufferSize = FileStream.Read(buffer, 0, blockSize);
            if (bufferSize == 0)
            {
                block = null;
                return false;
            }

            var finalBuffer = new byte[bufferSize];
            Array.Copy(buffer, finalBuffer, bufferSize);
            block = finalBuffer;
            return true;
        }

        private bool TryReadBlock(out StreamBlock block)
        {
            if (TryGetNextBlock(out var buffer))
            {
                block = new StreamBlock(buffer, _currentIndex++);
                return true;
            }

            block = new StreamBlock();
            return false;
        }

        private void RunRead()
        {
            while (!_readCompleted && !_cancellationRequired)
            {
                ReadBlock();

                lock (_startSyncRoot)
                {
                    Monitor.PulseAll(_startSyncRoot);
                }
            }
        }

        private void ReadBlock()
        {
            var hasBlock = TryReadBlock(out var block);
            if (!hasBlock)
            {
                _readCompleted = true;
                return;
            }

            _blocks.Eqnueue(block);
        }
    }
}