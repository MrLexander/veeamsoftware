using System.IO;
using System.IO.Compression;
using System.Threading;
using VeeamSoftware.Infrastructure.Model;
using VeeamSoftware.Threading;

namespace VeeamSoftware.Zip
{
    public sealed class DecompressStreamsHandler : StreamsHandlerBase
    {
        public DecompressStreamsHandler(string outputFile, SafeThreadPool safeThreadPool, int threadCount)
            : base(outputFile, safeThreadPool, threadCount)
        {
        }

        protected override void Operate() => DecompressInMemory();

        private void DecompressInMemory()
        {
            while (!BlockQueue.IsEmpty() || !FileReadCompleted)
            {
                lock (BlockQueueSyncRoot)
                {
                    while (BlockQueue.IsEmpty() && !FileReadCompleted)
                    {
                        Monitor.Wait(BlockQueueSyncRoot);
                    }
                }

                if (!BlockQueue.TryDequeue(out var block))
                {
                    continue;
                }

                var buffer = DecompressBlock(block);
                AddOperatedBytes(block.Block.Length);
                Buffer.Add(block.Index, buffer);

                lock (OperationSyncRoot)
                {
                    Monitor.PulseAll(OperationSyncRoot);
                }
            }
        }

        private static byte[] DecompressBlock(StreamBlock streamBlock)
        {
            var size = streamBlock.Block.Length;

            var compressedMemoryStream = new MemoryStream(streamBlock.Block);
            MemoryStream decompressedMemoryStream;
            using (decompressedMemoryStream = new MemoryStream())
            using (var writeMemoryStream = new GZipStream(compressedMemoryStream, CompressionMode.Decompress, false))
            {
                var buffer = new byte[size];
                int readCount;
                do
                {
                    readCount = writeMemoryStream.Read(buffer, 0, size);
                    if (readCount > 0)
                    {
                        decompressedMemoryStream.Write(buffer, 0, readCount);
                    }
                } while (readCount > 0);
            }

            return decompressedMemoryStream.ToArray();
        }
    }
}