﻿using System.IO;
using System.IO.Compression;
using System.Threading;
using VeeamSoftware.Infrastructure.Model;
using VeeamSoftware.Threading;

namespace VeeamSoftware.Zip
{
    public sealed class CompressStreamsHandler : StreamsHandlerBase
    {
        public CompressStreamsHandler(string outputFile, SafeThreadPool safeThreadPool, int threadCount)
            : base(outputFile, safeThreadPool, threadCount)
        {
        }

        protected override void Operate() => CompressInMemory();

        private void CompressInMemory()
        {
            while (!BlockQueue.IsEmpty() || !FileReadCompleted)
            {
                lock (BlockQueueSyncRoot)
                {
                    while (BlockQueue.IsEmpty() && !FileReadCompleted)
                    {
                        Monitor.Wait(BlockQueueSyncRoot);
                    }
                }

                if (!BlockQueue.TryDequeue(out var block))
                {
                    continue;
                }

                var buffer = CompressBlock(block);
                AddOperatedBytes(block.Block.Length);
                Buffer.Add(block.Index, buffer);

                lock (OperationSyncRoot)
                {
                    Monitor.PulseAll(OperationSyncRoot);
                }
            }
        }

        private static byte[] CompressBlock(StreamBlock streamBlock)
        {
            var size = streamBlock.Block.Length;

            var compressedMemoryStream = new MemoryStream();
            using (var writeMemoryStream = new GZipStream(compressedMemoryStream, CompressionMode.Compress, false))
            {
                writeMemoryStream.Write(streamBlock.Block, 0, size);
            }

            return compressedMemoryStream.ToArray();
        }
    }
}