﻿using System;
using System.IO;
using System.IO.Compression;
using Guard;
using JetBrains.Annotations;
using VeeamSoftware.Infrastructure.Model;
using VeeamSoftware.Threading;

namespace VeeamSoftware.Zip
{
    public sealed class Archiver : IProcessedInfoHandler, INotifyCompleted
    {
        [NotNull] private readonly string _inFilePath;
        [NotNull] private readonly string _outFilePath;
        [NotNull] private readonly SafeThreadPool _safeThreadPool;

        public event EventHandler OperationCompleted;

        public Archiver([NotNull] string inFilePath, [NotNull] string outFilePath, [NotNull] SafeThreadPool safeThreadPool)
        {
            ThrowIf.Argument.IsNull(safeThreadPool, nameof(safeThreadPool));
            ThrowIf.Argument.IsNull(safeThreadPool, nameof(safeThreadPool));
            ThrowIf.Argument.IsNull(inFilePath, nameof(inFilePath));
            ThrowIf.Argument.IsNull(outFilePath, nameof(outFilePath));

            var stream = File.Open(inFilePath, FileMode.Open);
            TotalBytes = stream.Length;
            stream.Dispose();

            _inFilePath = inFilePath;
            _outFilePath = outFilePath;
            _safeThreadPool = safeThreadPool;
        }

        public long OperatedBytesCount { get; private set; }
        public long TotalBytes { get; }

        public void Compress()
        {
            if (!CanCompress())
            {
                throw new FileLoadException("Can not compress file. It is incompressable");
            }

            CompressStreamsHandler compressStream;
            using (var readStream = new FileStream(_inFilePath, FileMode.Open))
            using (var fileReader = new FileReader(readStream, _safeThreadPool))
            using (compressStream = new CompressStreamsHandler(_outFilePath, _safeThreadPool, GetProcessCount(readStream.Length)))
            {
                var completed = false;
                while (!completed)
                {
                    var hasNext = fileReader.TryReadNext(out var block);
                    if (!hasNext)
                    {
                        completed = true;
                    }
                    else
                    {
                        compressStream.Operate(block);
                        OperatedBytesCount = compressStream.OperatedBytesCount;
                    }
                }
            }

            OnOperationCompleted();
        }

        public void Decompress()
        {
            using (var readStream = new FileStream(_inFilePath, FileMode.Open))
            using (var fileReader = new CompressedFileReader(readStream, _safeThreadPool))
            using (var decompressStream = new DecompressStreamsHandler(_outFilePath, _safeThreadPool, GetProcessCount(readStream.Length)))
            {
                var completed = false;
                while (!completed)
                {
                    var hasNext = fileReader.TryReadNext(out var block);
                    if (!hasNext)
                    {
                        completed = true;
                    }
                    else
                    {
                        decompressStream.Operate(block);
                        OperatedBytesCount = decompressStream.OperatedBytesCount;
                    }
                }
            }

            OnOperationCompleted();
        }

        private bool CanCompress()
        {
            long readedBytesTotalCount = 0;
            MemoryStream memoryStream;
            using (var fileReader = new FileStream(_inFilePath, FileMode.Open))
            using (memoryStream = new MemoryStream())
            using (var compressed = new GZipStream(memoryStream, CompressionMode.Compress))
            {
                for (var i = 0; i < 10; i++)
                {
                    const int bufferSize = CompressionConstants.DefaultSizeInBytes;
                    var buffer = new byte[bufferSize];
                    var readedBytesCount = fileReader.Read(buffer, 0, bufferSize);
                    readedBytesTotalCount += readedBytesCount;
                    if (readedBytesCount == 0)
                    {
                        break;
                    }

                    compressed.Write(buffer, 0, readedBytesCount);
                }
            }

            var memoryBuffer = memoryStream.ToArray();
            var canComress = memoryBuffer.LongLength < readedBytesTotalCount;
            return canComress;
        }

        private int GetProcessCount(long sourceFileSize)
        {
            const long fourGbInBytes = (long)4 * 1024 * 1024 * 1024;

            var processorCount = Environment.ProcessorCount;
            var fileParts = sourceFileSize / fourGbInBytes;
            fileParts += 2;

            return fileParts >= processorCount ? Convert.ToInt32(fileParts) : processorCount;
        }

        private void OnOperationCompleted() => OperationCompleted?.Invoke(this, EventArgs.Empty);
    }
}