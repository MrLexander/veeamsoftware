﻿using System;
using JetBrains.Annotations;

namespace VeeamSoftware.Zip
{
    public interface INotifyCompleted
    {
        [CanBeNull]
        event EventHandler OperationCompleted;
    }
}