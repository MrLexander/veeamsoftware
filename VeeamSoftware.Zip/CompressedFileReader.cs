using System.Collections.Generic;
using System.IO;
using VeeamSoftware.Infrastructure.Model;
using VeeamSoftware.Threading;

namespace VeeamSoftware.Zip
{
    internal sealed class CompressedFileReader : FileReader
    {
        internal CompressedFileReader(Stream stream, SafeThreadPool safeThreadPool)
            : base(stream, safeThreadPool)
        {
        }
        
        protected override bool TryGetNextBlock(out byte[] block)
        {
            if (IsEofReached)
            {
                block = null;
                return false;
            }
            
            var eofReached = false;
            var magicBytesFound = false;
            var result = new List<byte>();
            
            const int tempBufferBytesCount = 1;
            var tempBuffer = new byte[tempBufferBytesCount];
            var previousByte = default(byte);
            do
            {
                var readCount = FileStream.Read(tempBuffer, 0, tempBufferBytesCount);
                var readByte = tempBuffer[0];
                if (readCount == 0)
                {
                    eofReached = true;
                    continue;
                }

                if (result.Count > 2 && previousByte == CompressionConstants.GZipHeader.MagicNumberFirstPart && readByte == CompressionConstants.GZipHeader.MagicNumberSecondPart)
                {
                    magicBytesFound = true;
                    continue;
                }

                previousByte = readByte;
                result.Add(readByte);
            } while (!magicBytesFound && !eofReached);

            if (magicBytesFound)
            {
                const int magicNumberSize = CompressionConstants.GZipHeader.MagicNumberSize;
                FileStream.Seek(-magicNumberSize, SeekOrigin.Current);
            }
            
            block = result.ToArray();
            return true;
        }
    }
}