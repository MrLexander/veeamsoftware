﻿using System;
using JetBrains.Annotations;
using VeeamSoftware.Infrastructure.Model;

namespace VeeamSoftware.Zip
{
    public static class ArchiverExctensions
    {
        [NotNull]
        [MustUseReturnValue]
        public static Action CreateOperationAction([NotNull] this Archiver archiver, Operation operation)
        {
            Action operationAction;
            switch (operation)
            {
                case Operation.Compress:
                    operationAction = archiver.Compress;
                    break;
                case Operation.Decompress:
                    operationAction = archiver.Decompress;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(operation));
            }

            return operationAction;
        }
    }
}