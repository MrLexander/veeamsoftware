using System.Collections.Generic;
using System.IO;
using System.Threading;
using JetBrains.Annotations;
using VeeamSoftware.Collections;
using VeeamSoftware.Infrastructure.Model;
using VeeamSoftware.Scaffolding;
using VeeamSoftware.Threading;

namespace VeeamSoftware.Zip
{
    public abstract class StreamsHandlerBase : Disposable
    {
        [NotNull]
        protected readonly ConcurrentStreamBlockQueue BlockQueue;

        [NotNull]
        protected readonly object BlockQueueSyncRoot = new object();

        [NotNull]
        protected readonly ConcurrentDictionary<int, byte[]> Buffer;

        [NotNull]
        protected readonly object OperationSyncRoot = new object();

        [NotNull]
        [ItemNotNull]
        private readonly List<Thread> _operationThreads = new List<Thread>();

        [NotNull]
        private readonly Thread _publishThread;

        [NotNull]
        private readonly FileStream _writeFileStream;

        protected volatile bool FileReadCompleted;
        private volatile bool _operationsCompleted;

        private long _operatedBytesCount;
        private int _currentBlock;

        protected StreamsHandlerBase(string outputFile, SafeThreadPool safeThreadPool, int threadCount)
        {
            Buffer = new ConcurrentDictionary<int, byte[]>();
            BlockQueue = new ConcurrentStreamBlockQueue();

            _writeFileStream = new FileStream(outputFile, FileMode.Create);

            _publishThread = safeThreadPool.StartOperation(Publish);

            InitializeOperationThreads(safeThreadPool, threadCount);
        }

        internal long OperatedBytesCount => _operatedBytesCount;

        internal void Operate(StreamBlock block)
        {
            BlockQueue.Eqnueue(block);
            lock (BlockQueueSyncRoot)
            {
                Monitor.PulseAll(BlockQueueSyncRoot);
            }
        }

        protected void AddOperatedBytes(long value) => Interlocked.Add(ref _operatedBytesCount, value);

        protected override void FreeManagedResources()
        {
            FileReadCompleted = true;

            lock (BlockQueueSyncRoot)
            {
                Monitor.PulseAll(BlockQueueSyncRoot);
            }

            _operationThreads.ForEach(thread => thread.Join());

            _operationsCompleted = true;

            lock (OperationSyncRoot)
            {
                Monitor.PulseAll(OperationSyncRoot);
            }

            _publishThread.Join();

            _writeFileStream.Dispose();
        }

        protected abstract void Operate();

        private void InitializeOperationThreads(SafeThreadPool safeThreadPool, int threadsCount)
        {
            for (var i = 0; i < threadsCount; i++)
            {
                var thread = safeThreadPool.StartOperation(Operate);
                _operationThreads.Add(thread);
            }
        }

        private void Publish()
        {
            while (!_operationsCompleted || !Buffer.IsEmpty())
            {
                if (Buffer.TryDequeue(_currentBlock, out var buffer))
                {
                    _writeFileStream.Write(buffer, 0, buffer.Length);
                    Interlocked.Increment(ref _currentBlock);
                }
                else
                {
                    lock (OperationSyncRoot)
                    {
                        while (!_operationsCompleted && Buffer.IsEmpty())
                        {
                            Monitor.Wait(OperationSyncRoot);
                        }
                    }
                }
            }
        }
    }
}