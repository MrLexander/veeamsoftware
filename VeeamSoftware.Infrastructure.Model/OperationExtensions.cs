﻿using System;
using System.ComponentModel;
using System.Linq;
using Guard;
using JetBrains.Annotations;

namespace VeeamSoftware.Infrastructure.Model
{
    public static class OperationExtensions
    {
        public static Operation GetOperation([NotNull] this string command)
        {
            ThrowIf.Argument.IsNull(command, nameof(command));
            var members = typeof(Operation).GetMembers();

            foreach (var member in members)
            {
                var description = member.GetCustomAttributes(typeof(DescriptionAttribute), false).FirstOrDefault() as DescriptionAttribute;
                if (description?.Description?.Equals(command, StringComparison.OrdinalIgnoreCase) != true)
                {
                    continue;
                }

                var operation = (Operation) Enum.Parse(typeof(Operation), member.Name);
                ThrowIf.Variable.IsNull(operation, nameof(operation));

                return operation;
            }

            throw new ArgumentException(Strings.UnknownOperationMessage);
        }
    }
}