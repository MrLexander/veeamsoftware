﻿using Guard;
using JetBrains.Annotations;

namespace VeeamSoftware.Infrastructure.Model
{
    public struct StreamBlock
    {
        public int Index { get; }

        [NotNull]
        public byte[] Block { get; }

        public StreamBlock([NotNull] byte[] block, int index)
        {
            ThrowIf.Argument.IsNull(block, nameof(block));

            Block = block;
            Index = index;
        }
    }
}