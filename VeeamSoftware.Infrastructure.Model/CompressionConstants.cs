namespace VeeamSoftware.Infrastructure.Model
{
    public static class CompressionConstants
    {
        public const int DefaultSizeInBytes = 4 * 1024 * 1024;

        public static class GZipHeader
        {
            public const int MagicNumberSize = 2;
            public const int MagicNumberFirstPart = 31;
            public const int MagicNumberSecondPart = 139;    
        }
        
    }
}