﻿using System;
using System.Collections.Generic;
using System.IO;
using JetBrains.Annotations;

namespace VeeamSoftware.Infrastructure.Model
{
    public sealed class InputData
    {
        public InputData([NotNull] IList<string> args)
        {
            if (args == null || args.Count != 3)
            {
                throw new ArgumentException(Strings.IncorrectInputMessage);
            }

            var operation = args[0];
            if (operation == null)
            {
                throw new ArgumentException(Strings.UnknownOperationMessage);
            }
            Operation = operation.GetOperation();

            var inFile = args[1];
            if (inFile == null || !File.Exists(inFile))
            {
                throw new ArgumentException(Strings.InputFilePathRequiredMessage);
            }

            InputPath = inFile;


            var outFile = args[2];

            OutputPath = outFile ?? throw new ArgumentException(Strings.OutputFilePathRequiredMessage);
        }

        public Operation Operation { get; }

        [NotNull]
        public string InputPath { get; }

        [NotNull]
        public string OutputPath { get; }
    }
}