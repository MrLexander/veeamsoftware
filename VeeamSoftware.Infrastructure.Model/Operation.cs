﻿using System.ComponentModel;

namespace VeeamSoftware.Infrastructure.Model
{
    public enum Operation
    {
        [Description("compress")] Compress,
        [Description("decompress")] Decompress
    }
}