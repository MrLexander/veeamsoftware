﻿using System;
using System.IO;
using NUnit.Framework;

namespace VeeamSoftware.Infrastructure.Model.Tests
{
    [TestFixture]
    public sealed class InputDataTests
    {
        [Test]
        public void IncorectArgumentsCount_Throw()
        {
            var arguments = new[] {""};

            Assert.Throws<ArgumentException>(() =>
            {
                var _ = new InputData(arguments);
            });
        }

        [Test]
        public void IncorrectInputPath_Throw()
        {
            var arguments = new[] {"compress", "aaa", "bbb"};

            Assert.Throws<ArgumentException>(() =>
            {
                var _ = new InputData(arguments);
            });
        }

        [Test]
        public void IncorrectOutputPath_Throw()
        {
            var tempFileName = Path.GetTempFileName();
            var arguments = new[] {"compress", tempFileName, "bbb"};

            Assert.Throws<ArgumentException>(() =>
            {
                var _ = new InputData(arguments);
            });

            File.Delete(tempFileName);
        }

        [Test]
        public void InputCorrectData_Test()
        {
            var tempFileName = Path.GetTempFileName();
            var arguments = new[] {"compress", tempFileName, tempFileName};

            var inputData = new InputData(arguments);

            Assert.AreEqual(Operation.Compress, inputData.Operation);
            Assert.AreEqual(tempFileName, inputData.InputPath);
            Assert.AreEqual(tempFileName, inputData.OutputPath);

            File.Delete(tempFileName);
        }
    }
}