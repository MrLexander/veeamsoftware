﻿using System;
using NUnit.Framework;

namespace VeeamSoftware.Infrastructure.Model.Tests
{
    [TestFixture]
    public sealed class OperationExtensionsTests
    {
        [Test]
        public void Compress_CompressOperation()
        {
            var operation = "compress".GetOperation();

            Assert.AreEqual(Operation.Compress, operation);
        }

        [Test]
        public void Unknown_Throws()
        {
            Assert.Throws<ArgumentException>(() => "unknown".GetOperation());
        }
    }
}