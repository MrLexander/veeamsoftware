﻿using System;

namespace VeeamSoftware.Scaffolding
{
    public abstract class Disposable : IDisposable
    {
        private bool _disposed;

        public void Dispose()
        {
            Dispose(true);
        }

        ~Disposable()
        {
            Dispose(false);
        }

        private void Dispose(bool disposing)
        {
            if (_disposed)
            {
                return;
            }

            if (disposing)
            {
                FreeManagedResources();
            }

            FreeUnmanagedResources();

            _disposed = true;
            GC.SuppressFinalize(this);
        }

        protected virtual void FreeManagedResources()
        {
        }

        protected virtual void FreeUnmanagedResources()
        {
        }
    }
}