﻿using System.Collections.Generic;
using System.Threading;
using JetBrains.Annotations;

namespace VeeamSoftware.Collections
{
    public sealed class ConcurrentDictionary<TIndex, TValue> where TValue : class
    {
        [NotNull] private readonly Dictionary<TIndex, TValue> _blocks = new Dictionary<TIndex, TValue>();
        private readonly int _maxItemsCount;

        public ConcurrentDictionary()
        {
            _maxItemsCount = ConcurrentHelper.MaxItemsCount;
        }

        public bool IsEmpty()
        {
            lock (_blocks)
            {
                return _blocks.Count == 0;
            }
        }

        public void Add(TIndex index, TValue value)
        {
            lock (_blocks)
            {
                while (_blocks.Count >= _maxItemsCount)
                {
                    Monitor.Wait(_blocks);
                }

                _blocks.Add(index, value);
            }
        }

        public bool TryDequeue(TIndex index, out TValue value)
        {
            lock (_blocks)
            {
                if (!_blocks.TryGetValue(index, out var buffer))
                {
                    value = null;
                    return false;
                }

                _blocks.Remove(index);
                value = buffer;
                Monitor.PulseAll(_blocks);
            }

            return true;
        }
    }
}