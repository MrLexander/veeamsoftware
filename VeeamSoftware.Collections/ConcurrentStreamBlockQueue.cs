﻿using System.Collections.Generic;
using System.Threading;
using JetBrains.Annotations;
using VeeamSoftware.Infrastructure.Model;

namespace VeeamSoftware.Collections
{
    public sealed class ConcurrentStreamBlockQueue
    {
        [NotNull] private readonly Queue<StreamBlock> _blocks = new Queue<StreamBlock>();
        private readonly int _maxItemsCount;

        public ConcurrentStreamBlockQueue()
        {
            _maxItemsCount = ConcurrentHelper.MaxItemsCount;
        }

        public bool IsEmpty()
        {
            lock (_blocks)
            {
                return _blocks.Count == 0;
            }
        }

        [NotNull]
        public StreamBlock[] DequeueAll()
        {
            lock (_blocks)
            {
                var blocks = _blocks.ToArray();
                _blocks.Clear();

                Monitor.PulseAll(_blocks);
                return blocks;
            }
        }

        public void Eqnueue(StreamBlock block)
        {
            lock (_blocks)
            {
                while (_blocks.Count >= _maxItemsCount)
                {
                    Monitor.Wait(_blocks);
                }

                _blocks.Enqueue(block);
            }
        }

        public bool TryDequeue(out StreamBlock block)
        {
            lock (_blocks)
            {
                if (_blocks.Count == 0)
                {
                    block = default(StreamBlock);
                    return false;
                }

                block = _blocks.Dequeue();
                Monitor.PulseAll(_blocks);
            }

            return true;
        }
    }
}