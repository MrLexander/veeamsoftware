using System;
using Microsoft.VisualBasic.Devices;
using VeeamSoftware.Infrastructure.Model;

namespace VeeamSoftware.Collections
{
    public static class ConcurrentHelper
    {
        public static int MaxItemsCount
        {
            get
            {
                var memorySizeInBytes = new ComputerInfo().AvailablePhysicalMemory;
                var maxItemsCountUlong = memorySizeInBytes / 4 * CompressionConstants.DefaultSizeInBytes;
                if (maxItemsCountUlong > int.MaxValue)
                {
                    maxItemsCountUlong = int.MaxValue;
                }

                return Convert.ToInt32(maxItemsCountUlong);    
            }
        }
    }
}