﻿using System;
using System.Windows.Forms;
using Guard;
using JetBrains.Annotations;

namespace VeeamSoftware.Exceptions
{
    public sealed class ExceptionHandler
    {
        public ExceptionHandler()
        {
            AppDomain.CurrentDomain.UnhandledException += OnUnhandledException;
        }

        private void OnUnhandledException(object sender, [NotNull] UnhandledExceptionEventArgs exceptionArgs)
        {
            ThrowIf.Argument.IsNull(exceptionArgs, nameof(exceptionArgs));
            var exception = (Exception) exceptionArgs.ExceptionObject;
            ThrowIf.Variable.IsNull(exception, nameof(exception));
            HandleException(exception);
        }

        public void HandleException([NotNull] Exception exception)
        {
            ThrowIf.Argument.IsNull(exception, nameof(exception));
            MessageBox.Show(exception.Message, Strings.ExceptionThrownMessage, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
    }
}
