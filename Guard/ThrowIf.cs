﻿using JetBrains.Annotations;

namespace Guard
{
    public static class ThrowIf
    {
        public static class Variable
        {
            [ContractAnnotation("variable:null=>halt")]
            public static void IsNull<T>(T variable, [NotNull] string variableName)
            {
                if (variable == null)
                {
                    ThrowHelper.NullReference(variableName);
                }
            }
        }

        public static class Argument
        {
            [ContractAnnotation("argument:null=>halt")]
            public static void IsNull<T>(T argument, [NotNull] string argumentName)
            {
                if (argument == null)
                {
                    ThrowHelper.ArgumentNull(argumentName);
                }
            }
        }
    }
}