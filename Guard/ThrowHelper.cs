﻿using System;
using JetBrains.Annotations;

namespace Guard
{
    [PublicAPI]
    public static class ThrowHelper
    {
        [ContractAnnotation("=>halt")]
        public static void ArgumentNull(string argumentName)
        {
            throw new ArgumentNullException(argumentName);
        }

        [ContractAnnotation("=>halt")]
        public static void NullReference(string variableName)
        {
            throw new NullReferenceException(variableName);
        }
    }
}