﻿using System;
using System.Diagnostics;
using System.Threading;
using Guard;
using JetBrains.Annotations;
using VeeamSoftware.Threading;
using VeeamSoftware.Zip;

namespace VeeamSoftWare.Compresser
{
    internal sealed class ComputerLoadInfoSubscriber
    {
        [NotNull] private readonly PerformanceCounter _performanceCounterCpu;

        [NotNull] private readonly IProcessedInfoHandler _processedInfoHandler;
        private bool _exceptionThrown;
        private bool _completed;

        internal ComputerLoadInfoSubscriber([NotNull] IProcessedInfoHandler processedInfoHandler,
                                            [NotNull] SafeThreadPool safeThreadPool,
                                            [NotNull] INotifyCompleted notifyCompleted)
        {
            ThrowIf.Argument.IsNull(notifyCompleted, nameof(notifyCompleted));
            ThrowIf.Argument.IsNull(safeThreadPool, nameof(safeThreadPool));
            ThrowIf.Argument.IsNull(processedInfoHandler, nameof(processedInfoHandler));

            safeThreadPool.ExceptionThrown += (sender, args) => _exceptionThrown = true;
            notifyCompleted.OperationCompleted += (sender, args) => _completed = true;

            _processedInfoHandler = processedInfoHandler;

            _performanceCounterCpu = new PerformanceCounter("Processor", "% Processor Time", "_Total");
        }

        internal void StartOutInfo()
        {
            while (!_completed && !_exceptionThrown)
            {
                Console.Clear();
                double wrottenBytes = _processedInfoHandler.OperatedBytesCount;
                var completionPercent = wrottenBytes / _processedInfoHandler.TotalBytes * 100;
                var wrottenMBytes = wrottenBytes / 1024 / 1024;
                var progressMessage = $"{Strings.ProgressMessage}{completionPercent:0.##}% ({wrottenMBytes:0.##} Mb) {Strings.completed}";
                Console.WriteLine(progressMessage);
                OutCpuInfo();
                OutMemoryInfo();
                Thread.Sleep(1000);
            }
        }

        private void OutCpuInfo()
        {
            var cpuUsageMessage = $"{Strings.CpuUsage}{_performanceCounterCpu.NextValue():0.##}%";
            Console.WriteLine(cpuUsageMessage);
        }

        private void OutMemoryInfo()
        {
            const int bytesInMByte = 1024 * 1024;
            var usedMemory = Convert.ToDouble(GC.GetTotalMemory(false)) / bytesInMByte;

            var usedMemoryMessage = $"{Strings.UsedMemoryMessage}{usedMemory:0.##} Mb";
            Console.WriteLine(usedMemoryMessage);
        }
    }
}