﻿using System;
using System.Diagnostics;
using JetBrains.Annotations;
using VeeamSoftware.Exceptions;
using VeeamSoftware.Infrastructure.Model;
using VeeamSoftware.Threading;
using VeeamSoftware.Zip;

namespace VeeamSoftWare.Compresser
{
    internal static class Program
    {
        [STAThread]
        private static void Main([NotNull] string[] args)
        {
            var exceptionHandler = new ExceptionHandler();
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            try
            {
                var safeThreadPool = new SafeThreadPool(exceptionHandler);

                var inputData = new InputData(args);

                var archiver = new Archiver(inputData.InputPath, inputData.OutputPath, safeThreadPool);
                var operationAction = archiver.CreateOperationAction(inputData.Operation);
                var operationThread = safeThreadPool.StartOperation(operationAction);

                var computerLoadInfoSubscriber = new ComputerLoadInfoSubscriber(archiver, safeThreadPool, archiver);
                computerLoadInfoSubscriber.StartOutInfo();

                operationThread.Join();
            }
            catch (Exception exception)
            {
                exceptionHandler.HandleException(exception);
            }
            finally
            {
                // ReSharper disable once LocalizableElement
                Console.WriteLine($"{Strings.WastedTime} {stopwatch.Elapsed}");
            }
        }
    }
}