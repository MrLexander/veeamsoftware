﻿using System;
using System.Threading;
using Guard;
using JetBrains.Annotations;
using VeeamSoftware.Exceptions;

namespace VeeamSoftware.Threading
{
    public sealed class SafeThreadPool
    {
        [NotNull] private readonly ExceptionHandler _exceptionHandler;

        public event EventHandler ExceptionThrown;

        public SafeThreadPool([NotNull] ExceptionHandler exceptionHandler)
        {
            ThrowIf.Argument.IsNull(exceptionHandler, nameof(exceptionHandler));
            _exceptionHandler = exceptionHandler;
        }

        [NotNull]
        public Thread StartOperation([NotNull] Action action)
        {
            ThrowIf.Argument.IsNull(action, nameof(action));

            var thread = new Thread(() => SafeExecute(action));
            thread.Start();
            return thread;
        }

        private void SafeExecute([NotNull] Action action)
        {
            try
            {
                action();
            }
            catch (Exception exception)
            {
                OnExceptionThrown();
                _exceptionHandler.HandleException(exception);
            }
        }

        private void OnExceptionThrown()
        {
            ExceptionThrown?.Invoke(this, EventArgs.Empty);
        }
    }
}
